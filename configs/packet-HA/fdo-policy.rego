package istio.authz

import input.attributes.request.http as http_request
import input.parsed_path

default allow = false

read_operations := {
  "GET",
  "HEAD",
}

write_only_operations := {
#  "s3:DeleteObject",
#  "s3:DeleteObjects",
#  "s3:PutObject",
#  "s3:PutObjectAcl",
#  "s3:PutObjectLegalHold",
#  "s3:PutObjectLockConfiguration"
  "POST",
  "PUT",
  "DELETE",
}

write_operations := read_operations | write_only_operations

########################################################
# example of a JWT token claims
########################################################
#
#{
#  "input": {
#    "account": "<REDACTED>",
#    "action": "s3:ListBucket",
#    "bucket": "git-cache",
#    "conditions": <REDACTED>
#    "owner": false,
#    "object": "",
#    "claims": {
#      "accessKey": "<REDACTED>",
#      "exp": "1593596324",
#      "iat": 1593595394,
#      "iss": "gitlab.freedesktop.org",
#      "job_id": "3378898",
#      "jti": "<REDACTED>",
#      "namespace_id": "686",
#      "namespace_path": "bentiss",
#      "nbf": 1593595389,
#      "pipeline_id": "169859",
#      "project_id": "6576",
#      "project_path": "bentiss/test-minio",
#      "ref": "master",
#      "ref_protected": "false",
#      "ref_type": "branch",
#      "sub": "job_3378898",
#      "user_email": "benjamin.tissoires@gmail.com",
#      "user_id": "685",
#      "user_login": "bentiss"
#    }
#  }
#}


AWS_TYPE := {
  "AWS4-HMAC-SHA256",
  "AWS",
}

# allow any S3 authenticated action, ceph will deal with it itself
allow {
    AWS_TYPE[authorization_type]
}

authorization_type = parsed {
  parsed := split(http_request.headers.authorization, " ")[0]
}

allow {
  AWS_TYPE[input.parsed_query["X-Amz-Algorithm"][0]]
}

########################################################
# for non owners (JWT auth)
########################################################
read_repos := {
  "artifacts",
  "git-cache",
  "mesa-lava",
  "mesa-tracie-public",
  "mesa-tracie-results",
}

tracie_private_users := {
  "marge-bot",
  "tomeu",
}

# decode the token
token = parsed {
  authorization_type == "Bearer"
  jwt := split(http_request.headers.authorization, " ")[1]
  [_, decoded, _] := io.jwt.decode(jwt)
  parsed := object.union(decoded, {"project_name": trim_prefix(decoded.project_path, sprintf("%v/", [decoded.namespace_path]))})
}

# keep read for everyone on read_repos
allow = true {
  read_repos[parsed_path[0]]
  parsed_path[1] != "" # prevent directory listing
  read_operations[http_request.method]
}

# git cache

allow = true {
 parsed_path[0] == "git-cache"
 parsed_path[1] == token.namespace_path
 parsed_path[2] == token.project_name
 parsed_path[3] == concat(".", [token.project_name, "tar.gz"])
 write_operations[http_request.method]
}

# artifacts

allow = true {
 parsed_path[0] == "artifacts"
 parsed_path[1] == token.namespace_path
 parsed_path[2] == token.project_name
 parsed_path[3] == token.pipeline_id
 write_operations[http_request.method]
}

# Mesa CI: LAVA jobs allow writing a rootfs/kernel/etc from jobs, then reading them back from anywhere
allow = true {
 parsed_path[0] == "mesa-lava"
 parsed_path[1] == token.namespace_path
 parsed_path[2] == token.project_name
 write_operations[http_request.method]
}

# Mesa CI: Tracie trace files can be written from traces-db jobs, and read from anywhere
allow = true {
 parsed_path[0] == "mesa-tracie-public"
 token.project_path == "gfx-ci/tracie/traces-db"
 write_operations[http_request.method]
}

# Mesa CI: Tracie private trace files can be written from traces-db-private jobs
allow = true {
 parsed_path[0] == "mesa-tracie-private"
 token.project_path == "gfx-ci/tracie/traces-db-private"
 write_only_operations[http_request.method]
}

# Mesa CI: Tracie private trace files can be read from jobs triggered by an authorized user
allow = true {
 parsed_path[0] == "mesa-tracie-private"
 tracie_private_users[token.user_login]
 read_operations[http_request.method]
}

# Mesa CI: Tracie can be written from mesa/mesa jobs, and read from anywhere
allow = true {
 parsed_path[0] == "mesa-tracie-results"
 token.project_path == "mesa/mesa"
 write_operations[http_request.method]
}
