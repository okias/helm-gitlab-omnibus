## Installation & configuration of stable/grafana
## See requirements.yaml for current version
## Controlled by `global.grafana.enabled`

## Override and provide "bogus" administation secrets
## gitlab/gitlab-grafana provides overrides via shared-secrets
admin:
  existingSecret: bogus
env:
  GF_SECURITY_ADMIN_USER: bogus
  GF_SECURITY_ADMIN_PASSWORD: bogus
## This override allows gitlab/gitlab-grafana to completely override the secret
##   handling behavior of the upstream chart in combination with the above.
command: [ "sh", "-x", "/tmp/scripts/import-secret.sh" ]
## The following settings allow Grafana to dynamically create
## dashboards and datasources from configmaps. See
## https://github.com/helm/charts/tree/master/stable/grafana#sidecar-for-dashboards
sidecar:
  dashboards:
    enabled: true
    label: gitlab_grafana_dashboard
  datasources:
    enabled: true
    label: gitlab_grafana_datasource
## We generate and provide random passwords
## NOTE: the Secret & ConfigMap names are hard coded!
extraSecretMounts:
  - name: initial-password
    mountPath: /tmp/initial
    readOnly: true
    secretName: gitlab-grafana-initial-password
    defaultMode: 400
extraConfigmapMounts:
  - name: import-secret
    mountPath: /tmp/scripts
    configMap: fdo-grafana-{{ .Values.releaseName }}-import-secret
    readOnly: true
testFramework:
  enabled: false

resources:
  requests:
    memory: "2Gi"
    cpu: "1"
persistence:
  enabled: true
  storageClassName: standard

fullnameOverride: gitlab-{{ .Values.releaseName }}-grafana
