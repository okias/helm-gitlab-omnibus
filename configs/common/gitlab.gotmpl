{{ readFile "definitions.yaml" }}

# Default values for gitlab chart
controller:
  ingressClass: *ingressClass
nginx-ingress:
  controller:
    extraArgs:
      enable-ssl-chain-completion: "false"

gitlab:
  geo-logcursor:
    enabled: false
  gitlab-shell:
    enabled: true
  migrations:
    enabled: true
  sidekiq:
    enabled: true
    pods:
    - name: native-chart
  toolbox:
    enabled: true
  webservice:
    enabled: true
    registry:
      tokenIssuer: 'omnibus-gitlab-issuer'
    ingress:
      proxyConnectTimeout: '30'
      proxyBodySize: '0'
      annotations:
        nginx.ingress.kubernetes.io/proxy-send-timeout: "600"
        nginx.ingress.kubernetes.io/proxy-request-buffering: "off"
        nginx.ingress.kubernetes.io/use-regex: "true"
      tls:
        secretName: gitlab-tls
    rack_attack:
      git_basic_auth:
        enabled: false
        ip_whitelist: 
          - '127.0.0.1'
        maxretry: 10  # Limit the number of Git HTTP authentication attempts per IP$
        findtime: 360 # Reset the auth attempt counter per IP after 60 seconds$
        bantime: 3600 # Ban an IP for one hour (3600s) after too many auth attempts$
    minReplicas: 8
    maxReplicas: 8
  gitlab-grafana:
    ingress:
      tls:
        secretName: gitlab-tls
gitlab-runner:
  install: false
grafana:
  resources:
    requests:
      memory: "2Gi"
      cpu: "1"

prometheus:
  nodeExporter:
    enabled: true
  server:
    resources:
      requests:
        memory: "4Gi"
        cpu: "2"
postgresql:
  resources:
    requests:
      memory: "16Gi"
      cpu: "2"
  postgresqlDatabase: gitlab_production
  postgresqlExtendedConf:
    max_connections: 300
    max_wal_size: "1GB"
redis:
  master:
    resources:
      requests:
        memory: "8Gi"
        cpu: "2"
registry:
  enabled: true
  maintenance:
    readOnly:
      enabled: false
  ingress:
    enabled: true
  tokenIssuer: 'omnibus-gitlab-issuer'
  log:
    level: info
  hpa:
    minReplicas: 4
    maxReplicas: 4
upgradeCheck:
  enabled: true
