global:
  hosts:
    domain: freedesktop.org
  appConfig:
    omniauth:
      enabled: true
      allowSingleSignOn: true
      blockAutoCreatedUsers: false
      providers:
        - secret: gitlab-prod-freedesktop-omniauth-providers
          key: google
        - secret: gitlab-prod-freedesktop-omniauth-providers
          key: gitlab
        - secret: gitlab-prod-freedesktop-omniauth-providers
          key: github
        - secret: gitlab-prod-freedesktop-omniauth-providers
          key: twitter
    backups:
      # Disabled in the chart due to freedesktop/helm-gitlab-omnibus@35a9e9326ba7
      proxy_download: false
      bucket: 'fdo-gitlab-data-backup'
      tmpBucket: 'fdo-gitlab-tmp-backup-storage'
    uploads:
      bucket: 'fdo-gitlab-uploads'
      connection:
        secret: gitlab-prod-freedesktop-upload-gcs-key
        key: connection
    artifacts:
      bucket: 'fdo-gitlab-artifacts'
      connection:
        secret: freedesktop-prod-ceph-s3-key
        key: connection
    lfs:
      bucket: 'fdo-gitlab-lfs'
      connection:
        secret: gitlab-prod-freedesktop-lfs-gcs-key
        key: connection
    packages:
      bucket: 'fdo-gitlab-packages'
      connection:
        secret: gitlab-prod-freedesktop-package-gcs-key
        key: connection
  email:
    from: 'gitlab@gitlab.freedesktop.org'
  ingress:
    configureCertmanager: false
    tls:
      secretName: gitlab-tls
    annotations:
      certmanager.k8s.io/cluster-issuer: "freedesktop-prod-issuer"
  smtp:
    enabled:  true
    address: 'gabe.freedesktop.org'
    port: 5878
    user_name: 'gitlab@gitlab.freedesktop.org'
    domain: 'gitlab.freedesktop.org'
    authentication: 'login'
    starttls_auto: true
    tls: false
    openssl_verify_mode: 'peer'
    password:
      secret: gitlab-prod-freedesktop-smtp-secret
  gitaly:
    enabled: false
    external:
    - name: gitaly-0
      hostname: gitlab-prod-gitaly-0.new-cluster.svc
    - name: gitaly-1
      hostname: gitlab-prod-gitaly-1.new-cluster.svc
    - name: gitaly-2
      hostname: gitlab-prod-gitaly-2.new-cluster.svc
    - name: default
      hostname: gitlab-prod-gitaly-0.new-cluster.svc
  praefect:
    enabled: false
  omnibus:
    omnibusCpu: 6
    omnibusMemory: 18Gi
  operator:
    enabled: false
  pages:
    enabled: true
    host: pages.freedesktop.org
    port: 443
    https: true
    externalHttp:
    - 1.2.3.4
    artifactsServer: true
    objectStore:
      enabled: true
      bucket: fdo-gitlab-pages
      connection:
        secret: freedesktop-prod-ceph-s3-key
        key: connection
  psql:
    host: gitlab-prod-postgresql-postgresql
    serviceName: gitlab-prod-postgresql-postgresql
  redis:
    host: gitlab-prod-redis-master
