{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "fullAppName" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{/* temporary hack to get the correct name with our GKE deployment */}}
{{- if (eq .Values.provider "gke") -}}
{{- printf "gitlab-%s-%s" (last (mustRegexSplit "-" .Release.Name -2)) $name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- .Chart.Name -}}
{{- end -}}
{{- end -}}

{{/*
Create a default fully qualified release name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "fullReleaseName" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{/* temporary hack to get the correct name with our GKE deployment */}}
{{- if (eq .Values.provider "gke") -}}
{{- printf "gitlab-%s-%s" (last (mustRegexSplit "-" .Release.Name -2)) $name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- .Release.Name -}}
{{- end -}}
{{- end -}}

{{/*
Run "fullname" as if it was in another chart. This is an imperfect emulation, but close.

This is especially useful when you reference "fullname" services/pods which may or may not be easy to reconstruct.

Call:

```
{{- include "gitlab.otherGitlab.fullname" ( dict "context" . "chartName" "name-of-other-chart" ) -}}
```
*/}}
{{- define "gitlab.otherGitlab.fullname" -}}
{{- $ReleaseEnv := last (mustRegexSplit "-" .context.Release.Name -2) -}}
{{- printf "gitlab-%s-%s" $ReleaseEnv .chartName | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* define the gitlab-pages pod name */}}
{{- define "gitlab.pages_pod_name" -}}
{{- include "gitlab.otherGitlab.fullname" ( dict "context" . "chartName" "gitlab-pages-custom-domains" ) }}
{{- end -}}

{{- define "gitlab.application.labels" -}}
app.kubernetes.io/name: {{ .Release.Name }}
{{- end -}}

{{- define "gitlab.standardLabels" -}}
app: {{ template "name" . }}
chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
release: {{ .Release.Name }}
heritage: {{ .Release.Service }}
{{- if .Values.global.application.create }}
{{ include "gitlab.application.labels" . }}
{{- end -}}
{{- end -}}

{{/*
  Generates smtp settings for retrieving the password from gitlab-omnibus
*/}}
{{- define "freedesktop.smtp_password" -}}
{{ coalesce .Values.global.omnibus.smtpPassword .Values.smtp.password }}
{{- end -}}

{{/*
  Generates postgres settings for retrieving the password from gitlab-omnibus
*/}}
{{- define "freedesktop.postgres_password" -}}
{{ coalesce .Values.global.omnibus.postgresPassword .Values.psql.password }}
{{- end -}}

{{/*
  Generates omniauth settings for retrieving the secrets from gitlab-omnibus
*/}}
{{- define "freedesktop.oauthGoogleSecret" -}}
{{ coalesce .Values.global.omnibus.oauthGoogleSecret .Values.oauth.google.secret }}
{{- end -}}

{{- define "freedesktop.oauthGoogleId" -}}
{{ coalesce .Values.global.omnibus.oauthGoogleId .Values.oauth.google.id }}
{{- end -}}

{{- define "freedesktop.oauthGitLabSecret" -}}
{{ coalesce .Values.global.omnibus.oauthGitLabSecret .Values.oauth.gitlab.secret }}
{{- end -}}

{{- define "freedesktop.oauthGitLabId" -}}
{{ coalesce .Values.global.omnibus.oauthGitLabId .Values.oauth.gitlab.id }}
{{- end -}}

{{- define "freedesktop.oauthGitHubSecret" -}}
{{ coalesce .Values.global.omnibus.oauthGitHubSecret .Values.oauth.github.secret }}
{{- end -}}

{{- define "freedesktop.oauthGitHubId" -}}
{{ coalesce .Values.global.omnibus.oauthGitHubId .Values.oauth.github.id }}
{{- end -}}

{{- define "freedesktop.oauthTwitterSecret" -}}
{{ coalesce .Values.global.omnibus.oauthTwitterSecret .Values.oauth.twitter.secret }}
{{- end -}}

{{- define "freedesktop.oauthTwitterId" -}}
{{ coalesce .Values.global.omnibus.oauthTwitterId .Values.oauth.twitter.id }}
{{- end -}}

{{/*
  Generates gcs settings for retrieving the secrets from gitlab-omnibus
*/}}
{{- define "freedesktop.registryGCSBucket" -}}
{{ coalesce .Values.global.omnibus.registryGCSBucket .Values.gcs.registry.bucket }}
{{- end -}}

{{- define "freedesktop.registryGCSKey" -}}
{{ coalesce .Values.global.omnibus.registryGCSKey .Values.gcs.registry.key }}
{{- end -}}

{{- define "freedesktop.artifactGCSKey" -}}
{{ coalesce .Values.global.omnibus.artifactGCSKey .Values.gcs.artifacts.key }}
{{- end -}}

{{- define "freedesktop.backupGCSKey" -}}
{{ coalesce .Values.global.omnibus.backupGCSKey .Values.gcs.backups.key }}
{{- end -}}

{{- define "freedesktop.uploadGCSKey" -}}
{{ coalesce .Values.global.omnibus.uploadGCSKey .Values.gcs.uploads.key }}
{{- end -}}

{{- define "freedesktop.lfsGCSKey" -}}
{{ coalesce .Values.global.omnibus.lfsGCSKey .Values.gcs.lfs.key }}
{{- end -}}

{{- define "freedesktop.packageGCSKey" -}}
{{ coalesce .Values.global.omnibus.packageGCSKey .Values.gcs.packages.key }}
{{- end -}}

{{- define "freedesktop.backupLegacyGCSKey" -}}
{{ coalesce .Values.global.omnibus.backupLegacyGCSKey .Values.gcs.backupLegacy.key }}
{{- end -}}

{{- define "freedesktop.backupLegacyGCSSecret" -}}
{{ coalesce .Values.global.omnibus.backupLegacyGCSSecret .Values.gcs.backupLegacy.secret }}
{{- end -}}

{{/*
Returns the secret name for the Secret containing the TLS certificate and key.
Uses `ingress.tls.secretName` first and falls back to `global.ingress.tls.secretName`
if there is a shared tls secret for all ingresses.
*/}}
{{- define "webservice.tlsSecret" -}}
{{- $defaultName := (dict "secretName" "") -}}
{{- if .Values.global.ingress.configureCertmanager -}}
{{- $_ := set $defaultName "secretName" "gitlab-tls" -}}
{{- else -}}
{{- $_ := set $defaultName "secretName" (include "gitlab.wildcard-self-signed-cert-name" .) -}}
{{- end -}}
{{- pluck "secretName" .Values.ingress.tls .Values.global.ingress.tls $defaultName | first -}}
{{- end -}}

{{/* ######### cert-manager templates */}}

{{- define "gitlab.certmanager_annotations" -}}
{{- if (pluck "configureCertmanager" .Values.global.ingress .Values.ingress (dict "configureCertmanager" false) | first) -}}
cert-manager.io/issuer: "{{- include "gitlab.otherGitlab.fullname" ( dict "context" . "chartName" "issuer" ) -}}"
{{- end -}}
{{- end -}}

{{/*
Returns the nginx ingress class
*/}}
{{- define "gitlab.ingressclass" -}}
{{- pluck "class" .Values.global.ingress (dict "class" (printf "%s-nginx" .Release.Name)) | first -}}
{{- end -}}

{{/*
Detect if `.Values.ingress.tls.enabled` is set
Returns `ingress.tls.enabled` if it is a boolean,
Returns `global.ingress.tls.enabled` if it is a boolean, and `ingress.tls.enabled` is not.
Return true in any other case.
*/}}
{{- define "gitlab.ingress.tls.enabled" -}}
{{- $globalSet := and (hasKey .Values.global.ingress "tls") (and (hasKey .Values.global.ingress.tls "enabled") (kindIs "bool" .Values.global.ingress.tls.enabled)) -}}
{{- $localSet := and (hasKey .Values.ingress "tls") (and (hasKey .Values.ingress.tls "enabled") (kindIs "bool" .Values.ingress.tls.enabled)) -}}
{{- if $localSet }}
{{-   .Values.ingress.tls.enabled }}
{{- else if $globalSet }}
{{-  .Values.global.ingress.tls.enabled }}
{{- else }}
{{-   true }}
{{- end -}}
{{- end -}}
