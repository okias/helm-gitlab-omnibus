include:
  - project: 'freedesktop/ci-templates'
    ref: 14731f78c23c7b523a85a26a068ade9ac1ecd2f3
    file: '/templates/alpine.yml'

variables:
  FDO_UPSTREAM_REPO: freedesktop/helm-gitlab-omnibus
  FDO_DISTRIBUTION_TAG: 2022-06-07.1
  FDO_DISTRIBUTION_PACKAGES: >-
    curl
    jq
    perl
    python3
    git
    py3-pip

container-prep:
  extends:
    - .fdo.container-build@alpine
  variables:
    FDO_DISTRIBUTION_EXEC: bash .gitlab-ci/prep.sh
  except:
    - schedules


.lint_command: &lint_command
  - helmfile -e lint lint

.template_command: &template_command |
  helmfile -e test template > $OUT_FILE

lint:
  extends:
    - .fdo.distribution-image@alpine
  script:
    - *lint_command
  except:
    - schedules
  needs:
    - container-prep

check_upgrade:
  extends:
    - .fdo.distribution-image@alpine
  script:
    - helm repo update

    # fetch upstream informations
    # we change a little bit the given json to accomadate helm 2 and 3
    - GITLAB_JSON_VERSIONS=$(helm search repo -o json | tr '[:upper:]' '[:lower:]' | sed 's/app_version/appversion/g' | jq '.[] | select(.name == "gitlab/gitlab")')
    - CHART_VERSION=$(echo $GITLAB_JSON_VERSIONS | jq -r '.version')
    - GITLAB_VERSION=$(echo $GITLAB_JSON_VERSIONS | jq -r '.appversion')

    # first generate our current config
    - export OUT_FILE=current-deployment.yaml
    - *template_command

    # then apply the new config
    - echo $CHART_VERSION > gitlabVersion

    # exit if there is no new version (git diff returns 0)
    - git diff --exit-code && exit 0 || true

    # commit our changes
    - git config user.email "16171-fdo-helm-bot@users.noreply.gitlab.freedesktop.org"
    - git config user.name "FDO helm bot"
    - BRANCH="upgrade-to-${GITLAB_VERSION}"
    - git branch -D $BRANCH || true
    - git checkout -b $BRANCH
    - git add gitlabVersion
    - git commit -m "upgrade-to-${GITLAB_VERSION}"

    # check the new config
    - *lint_command
    - export OUT_FILE=future-deployment.yaml
    - *template_command

    # compare the 2
    - DIFF_OUTPUT=$(diff -u current-deployment.yaml future-deployment.yaml || true)
    - echo "$DIFF_OUTPUT"

    # create a new MR if required:
    # https://about.gitlab.com/blog/2017/09/05/how-to-automatically-create-a-new-mr-on-gitlab-with-gitlab-ci/
      # Look which is the default branch
    - echo ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}
    - curl "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}" | jq
    - TARGET_BRANCH=$(curl --silent "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}" | jq -r '.default_branch')
    - echo $TARGET_BRANCH

    - |
      TITLE="WIP: A new version of gitlab (${GITLAB_VERSION}) is available"

      # The description of our new MR, we want to remove the branch after the MR has
      # been closed
    - |
      cat > body.yaml <<EOF
      id: ${CI_PROJECT_ID}
      source_branch: ${BRANCH}
      target_branch: ${TARGET_BRANCH}
      remove_source_branch: true
      title: "${TITLE}"
      description: |
        current diff:
        \`\`\`diff
      $(echo "${DIFF_OUTPUT}" | sed 's/^/  /')
        \`\`\`
      EOF

    - LISTMR=$(curl --silent "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests?state=opened")

      # check if our MR has already been opened
      # if the following returns true, we bail out,
      # if not, we continue and open the MR
    - echo $LISTMR | jq -e -r ".[] | select(.title == \"$TITLE\")" && exit 0 || true

    # push the commit to the upstream tree
    - git push https://gitlab-ci-token:${BOT_API_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git $BRANCH

    - |
      cat body.yaml | yq . | \
        curl -X POST "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests" \
             --header "PRIVATE-TOKEN:${BOT_API_TOKEN}" \
             --header "Content-Type: application/json" \
             --data @-
  only:
    - schedules
  artifacts:
    expire_in: 1w
    paths:
    - current-deployment.yaml
    - future-deployment.yaml
    - body.yaml
    when: always
